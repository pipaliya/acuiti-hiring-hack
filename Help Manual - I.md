# Help Doc  // Reverse Engineering Codecs


> We're glad you are here, to make things swift for you, we have drafted a set of helpful links that will ease your work on the Acuiti Video Decoding project, however you will be the Steve Woz/Ada Lovelace of this project.


> If you’re going to make a space in Reverse engineering, you have to think like one. Which means, trying out all the possible tools and utilities to decode the video formats.


> So, here is the guideline of this week to win Acuiti Hiring Hack. This challenge distills major disciplines of professional reverse engineering mindset into short, objectively measurable exercises. The focus areas that all the levels of challenge extensions tend to measure are converting and merging video files through programming, and video tools tradecraft.


>  In these guideline, you’ll find everything you need to win competition which will be helpful for all the levels:


`Programming shall help automate the manual steps taken to convert/merge videos. Any favourite language (preferably C++, Java, PHP, Python, Bash Script)`


* **ffmpeg**

- Link — [/tricks-ffmpeg](http://linuxaria.com/howto/tricks-ffmpeg)
- Link — [/ffmpeg-tricks-you-should-know-about](http://codelinks.pachanka.org/post/33715573358/ffmpeg-tricks-you-should-know-about)

* **gstreamer**

- Link — [gstreamer/features](https://gstreamer.freedesktop.org/features/)

* $ **binwalk / strings / file**

- Link — [/binwalk](http://binwalk.org/features/)
- Link — [/extract-files-from-a-bin-firmware](http://reverseengineering.stackexchange.com/questions/8063/extract-files-from-a-bin-firmware)
- Link — [Analyze_Firmware_File](https://www.owasp.org/index.php/IoT_Firmware_Analysis)
- Link — [/strings](http://www.thegeekstuff.com/2010/11/strings-command-examples/)
- Link — [file](http://www.computerhope.com/unix/ufile.htm)

* **To understand decoding**

- Link — [/openh264 > Decoder Features](https://github.com/cisco/openh264)

**Note:** The developer who cracks the videos first will get the prize & recognition. If you still need clarifications you can chat with us or email us at mayur(at)hackerearth.com.

Now, time to work.

— Mayur Pipaliya

