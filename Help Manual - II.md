Hi Hackers,

To help you successfully complete the Acuiti decryption of unknown codecs project, here is another Help doc to excel in the challenge of video extraction.  You can submit your source code by  20th June 11 AM. 

## **Reverse Engineering / Acuiti Help doc to excel in the challenge of video extraction for Level - 2 Help / H264**

- Applying ffmpeg to the chunk will yield mixture of jumbled 4 stream.
- Player is playing 4-feed video from H264 video.
- Player reads specific length/flag of size: 24
- Sample Offset/Length/Block along with the pattern to help you create chunks.  
        Pastebin: [http://pastebin.com/LXeXEP6S][1]  
        Imgur: [https://i.imgur.com/Xhu9skt.png][2]
- Tools:  
        **Process Monitor to observe pattern**  
        **HxD (hex editor) to browse through the decimal and offset values**


———————————————————————————————————


## Here is the list of help to kickstart the reverse engineering of **Level - 3 [EXE}**
	
- Use **binwalk** to extract exe.  
          $ binwalk -e “filename.exe”
- **Search** for anomalies and similarities in extracted data.  
         There are **17 video feeds** hidden inside the binary  
         Once the binwalk extraction is done, observe the anomaly/pattern in the extracted files. There shall be **~17 files with         variable size.**
- **Sort** that extracted data based on similarities (in terms of file size).
- Apply **$ ffmpeg** on these sorted files to convert this extracted data to mp4.
- You shall be able to see at least **4 functional mp4 video files** comprising multiple feed.
- Convert these multiple functional feeds into **chunk of videos** by recognising the unique intervals after which video feed keeps changing  
[http://stackoverflow.com/a/19300561][3]

You can review the previously sent [Help Doc here.][4] 


  [1]: http://pastebin.com/LXeXEP6S
  [2]: https://i.imgur.com/Xhu9skt.png
  [3]: http://stackoverflow.com/a/19300561
  [4]: https://docs.google.com/document/d/1l8StNYraTQIwMBHB_TnxK3tYMRm01roAzFlTYaBcjsk/edit

